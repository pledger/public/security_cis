# Security CIS

This file contains the main instruction to build and run SecurityCIS for the [Pledger system](https://pledger-project.eu/)


### Development

SecurityCIS is based on [JHipster 6.10.5](https://www.jhipster.tech/documentation-archive/v6.10.5) with [this model file](jhipster-jdl.jdl). Before you can build this project, you must install and configure the following dependencies on your machine with minimal version highlighted:

- [OpenJDK](https://openjdk.java.net/) or [AdoptJDK](https://adoptopenjdk.net/) v1.8: Java Developer Kit is used to run the project services
- [Maven](https://maven.apache.org/) v3.3: Maven is used to build the project
- [MySQL](https://www.mysql.com/downloads/) v5.6: MySQL is used to persist configuration and DSS data
- [Node.js](https://nodejs.org/en/) v12.16: Node is used to run a development web server and build the project.
- [npm](https://docs.npmjs.com/) v6.14: npm is used to install Node dependencies and to launch the development front end service

After installing Node, run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

```
npm install
```

Then, before you run the project, you need a reference to [ConfServiceDSS](https://gitlab.com/pledger) and [Kafka](https://kafka.apache.org/) service running. These topics need to be present:
- security_infra_cis

Moreover, Kafka environment variables need to be set, for example:

```
export KAFKA_BOOTSTRAP_SERVERS=localhost:9092
export KAFKA_CONSUMER_KEY_DESERIALIZER=org.apache.kafka.common.serialization.StringDeserializer
export KAFKA_CONSUMER_VALUE_DESERIALIZER=org.apache.kafka.common.serialization.JsonDeserializer
export KAFKA_PRODUCER_KEY_SERIALIZER=org.apache.kafka.common.serialization.StringSerializer 
export KAFKA_PRODUCER_VALUE_SERIALIZER=org.springframework.kafka.support.serializer.JsonSerializer
export CONFSERVICE_URL=confservice_url
export CONFSERVICE_USER=confservice_username
export CONFSERVICE_PASS=confservice_password

```

After setting up the environment variables, you can run the following commands in two separate terminals for development, to have browser auto-refresh when files change on the hard drive (backend will listen on port 8080, frontend on port 9000)

```

mvn -Dspring.profiles.active=prod -DskipTests=true

npm start
```

The default MySQL configuration expects MySQL to be running on localhost:3306 with user/pass root/root with a schema "security_cis" already created. MySQL url, user and pass can be overridden with the following additional environment variables:

```
export SPRING_DATASOURCE_URL=jdbc:mysql://localhost:3306/security_cis
export SPRING_DATASOURCE_USERNAME=root
export SPRING_DATASOURCE_PASSWORD=root
```

### Packaging and launching as jar

To build the final jar and optimize SecurityCIS for production, run:

```

mvn -Pprod clean package -DskipTests=true 


```

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files. 

To ensure everything worked, run (after adding the environment variables above):

```

java -jar target/*.jar -Pprod


```

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser and login with root/test


### Packaging and launching as a container

To package your SecurityCIS as a local Docker image, run:

```

mvn package -Pprod jib:dockerBuild -DskipTests=true -Dimage=securitycis 

```

To package your SecurityCIS as a Docker image and push to a Docker registry, run:

```

mvn package -Pprod jib:dockerBuild -DskipTests=true -Djib.allowInsecureRegistries=true  -Dimage=myregistry/securitycis


```

To run your SecurityCIS from a Docker image, run (adding the environment variables above with "-e" BEFORE the "-p" option):

```

docker run -it -e AAA=BBB..  -p8080:8080 securitycis


```
or environment variables can be placed in a file (like [this](securitycis.env)) and run

```
docker run -it --env-file securitycis.env -p8080:8080 securitycis
```

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser and login with security/admin



######This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 871536.


