
HOWTO install Kubei
kubectl apply -f https://raw.githubusercontent.com/Portshift/kubei/master/deploy/kubei.yaml
kubectl -n kubei port-forward $(kubectl -n kubei get pods -lapp=kubei -o jsonpath='{.items[0].metadata.name}') 8080
#kubectl edit svc kubei -n kubei #nodeport 31288/31289


HOWTO install kube-bench (on each infrastructure node)
sudo docker run --rm -v `pwd`:/host aquasec/kube-bench:latest install ./kube-bench
sudo apt install python3-pip -y
pip3 install kafka-python


HOWTO configure send_kafka_prod.sh 
to send data on INTRA Kafka, you need change the script or just provide ca.pem, cert.pem, key.pem and set the password
this script is by default expecting certificates in a subfolder named "private" 
NOTE: THIS STEP IS MANDATORY TO MAKE THE SCRIPT WORK    

If you have JKS, you can get ca, cert and key from it with keytool (and openssl)
sudo apt install openjdk-11-jre-headless -y
sudo apt install libssl-dev

1) extract ca.pem
keytool -exportcert -alias caroot -keystore kafka.client.truststore.jks -rfc -file ca.pem

2) extract cert.pem
keytool -exportcert -alias cert -keystore kafka.client.keystore.jks -rfc -file cert.pem

3) extract key.pem
keytool -importkeystore -srckeystore kafka.client.keystore.jks -destkeystore new-store.p12 -deststoretype PKCS12
openssl pkcs12 -in new-store.p12 -nodes -nocerts -out key.pem
rm new-store.p12



HOWTO configure kube-bench.sh to run periodically (on Ubuntu)
cd /home/ubuntu
mkdir .config
cd .config
mkdir systemd
cd systemd
mkdir user
cd /home/ubuntu

#cp kube-bench.service /home/ubuntu/.config/systemd/user
#cp kube-bench.timer /home/ubuntu/.config/systemd/user
systemctl --user enable kube-bench.service kube-bench.timer
systemctl --user start kube-bench.timer  
  

  
MAIN STEPS TO CONFIGURE A NODE TO SEND CIS SECURITY BENCHMARKS
1) install kube-bench on each node (see "HOWTO install kube-bench" above)
2) copy kube-bench.sh and send_kafka_prod.sh on each node
3) configure send_kafka_prod.sh with proper certificates (see "HOWTO configure send_kafka_prod.sh" above) 
4) configure kube-bench.sh to run periodically (see "HOWTO configure kube-bench.sh to run periodically" above)


NOTE: with few adjustments, these steps work also for Docker nodes using docker/docker-bench-security (https://github.com/docker/docker-bench-security)
