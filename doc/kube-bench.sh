#!/bin/bash

sudo /home/ubuntu/kube-bench --json --noremediations --nosummary > /home/ubuntu/master_report.json
DATA="{\"hostname\": \"`hostname`\", "
DATA+=`cat /home/ubuntu/master_report.json | cut -c2-`
echo $DATA > /home/ubuntu/master_report_extended.json

/home/ubuntu/send_kafka_prod.sh -t security_infra_cis -f /home/ubuntu/master_report_extended.json
