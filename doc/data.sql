use security_cis;
UNLOCK TABLES;

SET FOREIGN_KEY_CHECKS=0;
delete from `jhi_user`;
delete from `jhi_user_authority`;
delete from `jhi_authority`;

INSERT INTO `jhi_user` (`id`, `login`, `password_hash`, `first_name`, `last_name`, `email`, `image_url`, `activated`, `lang_key`, `activation_key`, `reset_key`, `created_by`, `created_date`, `reset_date`, `last_modified_by`, `last_modified_date`) VALUES 
(1,'security','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','security','security','security@localhost','',_binary '','en',NULL,NULL,'system',NULL,NULL,'system',NULL);

INSERT INTO `jhi_user_authority` (`user_id`, `authority_name`) VALUES 
(1,'ROLE_ADMIN'),
(1,'ROLE_USER');

INSERT INTO `jhi_authority` (`name`) VALUES 
('ROLE_ADMIN'),
('ROLE_USER');

SET FOREIGN_KEY_CHECKS=1;