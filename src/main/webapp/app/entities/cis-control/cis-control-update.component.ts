import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICISControl, CISControl } from 'app/shared/model/cis-control.model';
import { CISControlService } from './cis-control.service';
import { ICISReport } from 'app/shared/model/cis-report.model';
import { CISReportService } from 'app/entities/cis-report/cis-report.service';

@Component({
  selector: 'jhi-cis-control-update',
  templateUrl: './cis-control-update.component.html',
})
export class CISControlUpdateComponent implements OnInit {
  isSaving = false;
  cisreports: ICISReport[] = [];

  editForm = this.fb.group({
    id: [],
    version: [],
    type: [],
    description: [],
    totalPass: [],
    totalFail: [],
    totalWarn: [],
    totalInfo: [],
    report: [],
  });

  constructor(
    protected cISControlService: CISControlService,
    protected cISReportService: CISReportService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cISControl }) => {
      this.updateForm(cISControl);

      this.cISReportService.query().subscribe((res: HttpResponse<ICISReport[]>) => (this.cisreports = res.body || []));
    });
  }

  updateForm(cISControl: ICISControl): void {
    this.editForm.patchValue({
      id: cISControl.id,
      version: cISControl.version,
      type: cISControl.type,
      description: cISControl.description,
      totalPass: cISControl.totalPass,
      totalFail: cISControl.totalFail,
      totalWarn: cISControl.totalWarn,
      totalInfo: cISControl.totalInfo,
      report: cISControl.report,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cISControl = this.createFromForm();
    if (cISControl.id !== undefined) {
      this.subscribeToSaveResponse(this.cISControlService.update(cISControl));
    } else {
      this.subscribeToSaveResponse(this.cISControlService.create(cISControl));
    }
  }

  private createFromForm(): ICISControl {
    return {
      ...new CISControl(),
      id: this.editForm.get(['id'])!.value,
      version: this.editForm.get(['version'])!.value,
      type: this.editForm.get(['type'])!.value,
      description: this.editForm.get(['description'])!.value,
      totalPass: this.editForm.get(['totalPass'])!.value,
      totalFail: this.editForm.get(['totalFail'])!.value,
      totalWarn: this.editForm.get(['totalWarn'])!.value,
      totalInfo: this.editForm.get(['totalInfo'])!.value,
      report: this.editForm.get(['report'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICISControl>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICISReport): any {
    return item.id;
  }
}
