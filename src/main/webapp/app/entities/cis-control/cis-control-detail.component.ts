import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICISControl } from 'app/shared/model/cis-control.model';

@Component({
  selector: 'jhi-cis-control-detail',
  templateUrl: './cis-control-detail.component.html',
})
export class CISControlDetailComponent implements OnInit {
  cISControl: ICISControl | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cISControl }) => (this.cISControl = cISControl));
  }

  previousState(): void {
    window.history.back();
  }
}
