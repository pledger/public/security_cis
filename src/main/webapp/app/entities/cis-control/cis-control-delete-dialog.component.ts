import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICISControl } from 'app/shared/model/cis-control.model';
import { CISControlService } from './cis-control.service';

@Component({
  templateUrl: './cis-control-delete-dialog.component.html',
})
export class CISControlDeleteDialogComponent {
  cISControl?: ICISControl;

  constructor(
    protected cISControlService: CISControlService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.cISControlService.delete(id).subscribe(() => {
      this.eventManager.broadcast('cISControlListModification');
      this.activeModal.close();
    });
  }
}
