import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SecurityCisSharedModule } from 'app/shared/shared.module';
import { CISControlComponent } from './cis-control.component';
import { CISControlDetailComponent } from './cis-control-detail.component';
import { CISControlUpdateComponent } from './cis-control-update.component';
import { CISControlDeleteDialogComponent } from './cis-control-delete-dialog.component';
import { cISControlRoute } from './cis-control.route';

@NgModule({
  imports: [SecurityCisSharedModule, RouterModule.forChild(cISControlRoute)],
  declarations: [CISControlComponent, CISControlDetailComponent, CISControlUpdateComponent, CISControlDeleteDialogComponent],
  entryComponents: [CISControlDeleteDialogComponent],
})
export class SecurityCisCISControlModule {}
