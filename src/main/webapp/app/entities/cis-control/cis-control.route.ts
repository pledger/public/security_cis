import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICISControl, CISControl } from 'app/shared/model/cis-control.model';
import { CISControlService } from './cis-control.service';
import { CISControlComponent } from './cis-control.component';
import { CISControlDetailComponent } from './cis-control-detail.component';
import { CISControlUpdateComponent } from './cis-control-update.component';

@Injectable({ providedIn: 'root' })
export class CISControlResolve implements Resolve<ICISControl> {
  constructor(private service: CISControlService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICISControl> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((cISControl: HttpResponse<CISControl>) => {
          if (cISControl.body) {
            return of(cISControl.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CISControl());
  }
}

export const cISControlRoute: Routes = [
  {
    path: '',
    component: CISControlComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'CISControls',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CISControlDetailComponent,
    resolve: {
      cISControl: CISControlResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISControls',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CISControlUpdateComponent,
    resolve: {
      cISControl: CISControlResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISControls',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CISControlUpdateComponent,
    resolve: {
      cISControl: CISControlResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISControls',
    },
    canActivate: [UserRouteAccessService],
  },
];
