import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICISControl } from 'app/shared/model/cis-control.model';

type EntityResponseType = HttpResponse<ICISControl>;
type EntityArrayResponseType = HttpResponse<ICISControl[]>;

@Injectable({ providedIn: 'root' })
export class CISControlService {
  public resourceUrl = SERVER_API_URL + 'api/cis-controls';

  constructor(protected http: HttpClient) {}

  create(cISControl: ICISControl): Observable<EntityResponseType> {
    return this.http.post<ICISControl>(this.resourceUrl, cISControl, { observe: 'response' });
  }

  update(cISControl: ICISControl): Observable<EntityResponseType> {
    return this.http.put<ICISControl>(this.resourceUrl, cISControl, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICISControl>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICISControl[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
