import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SecurityCisSharedModule } from 'app/shared/shared.module';
import { CISResultComponent } from './cis-result.component';
import { CISResultDetailComponent } from './cis-result-detail.component';
import { CISResultUpdateComponent } from './cis-result-update.component';
import { CISResultDeleteDialogComponent } from './cis-result-delete-dialog.component';
import { cISResultRoute } from './cis-result.route';

@NgModule({
  imports: [SecurityCisSharedModule, RouterModule.forChild(cISResultRoute)],
  declarations: [CISResultComponent, CISResultDetailComponent, CISResultUpdateComponent, CISResultDeleteDialogComponent],
  entryComponents: [CISResultDeleteDialogComponent],
})
export class SecurityCisCISResultModule {}
