import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICISResult } from 'app/shared/model/cis-result.model';
import { CISResultService } from './cis-result.service';

@Component({
  templateUrl: './cis-result-delete-dialog.component.html',
})
export class CISResultDeleteDialogComponent {
  cISResult?: ICISResult;

  constructor(protected cISResultService: CISResultService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.cISResultService.delete(id).subscribe(() => {
      this.eventManager.broadcast('cISResultListModification');
      this.activeModal.close();
    });
  }
}
