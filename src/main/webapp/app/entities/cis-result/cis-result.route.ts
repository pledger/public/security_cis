import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICISResult, CISResult } from 'app/shared/model/cis-result.model';
import { CISResultService } from './cis-result.service';
import { CISResultComponent } from './cis-result.component';
import { CISResultDetailComponent } from './cis-result-detail.component';
import { CISResultUpdateComponent } from './cis-result-update.component';

@Injectable({ providedIn: 'root' })
export class CISResultResolve implements Resolve<ICISResult> {
  constructor(private service: CISResultService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICISResult> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((cISResult: HttpResponse<CISResult>) => {
          if (cISResult.body) {
            return of(cISResult.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CISResult());
  }
}

export const cISResultRoute: Routes = [
  {
    path: '',
    component: CISResultComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'CISResults',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CISResultDetailComponent,
    resolve: {
      cISResult: CISResultResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISResults',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CISResultUpdateComponent,
    resolve: {
      cISResult: CISResultResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISResults',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CISResultUpdateComponent,
    resolve: {
      cISResult: CISResultResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISResults',
    },
    canActivate: [UserRouteAccessService],
  },
];
