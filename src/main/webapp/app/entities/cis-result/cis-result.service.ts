import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICISResult } from 'app/shared/model/cis-result.model';

type EntityResponseType = HttpResponse<ICISResult>;
type EntityArrayResponseType = HttpResponse<ICISResult[]>;

@Injectable({ providedIn: 'root' })
export class CISResultService {
  public resourceUrl = SERVER_API_URL + 'api/cis-results';

  constructor(protected http: HttpClient) {}

  create(cISResult: ICISResult): Observable<EntityResponseType> {
    return this.http.post<ICISResult>(this.resourceUrl, cISResult, { observe: 'response' });
  }

  update(cISResult: ICISResult): Observable<EntityResponseType> {
    return this.http.put<ICISResult>(this.resourceUrl, cISResult, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICISResult>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICISResult[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
