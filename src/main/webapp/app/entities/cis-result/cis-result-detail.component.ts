import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICISResult } from 'app/shared/model/cis-result.model';

@Component({
  selector: 'jhi-cis-result-detail',
  templateUrl: './cis-result-detail.component.html',
})
export class CISResultDetailComponent implements OnInit {
  cISResult: ICISResult | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cISResult }) => (this.cISResult = cISResult));
  }

  previousState(): void {
    window.history.back();
  }
}
