import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICISResult, CISResult } from 'app/shared/model/cis-result.model';
import { CISResultService } from './cis-result.service';
import { ICISControl } from 'app/shared/model/cis-control.model';
import { CISControlService } from 'app/entities/cis-control/cis-control.service';

@Component({
  selector: 'jhi-cis-result-update',
  templateUrl: './cis-result-update.component.html',
})
export class CISResultUpdateComponent implements OnInit {
  isSaving = false;
  ciscontrols: ICISControl[] = [];

  editForm = this.fb.group({
    id: [],
    number: [],
    description: [],
    status: [],
    control: [],
  });

  constructor(
    protected cISResultService: CISResultService,
    protected cISControlService: CISControlService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cISResult }) => {
      this.updateForm(cISResult);

      this.cISControlService.query().subscribe((res: HttpResponse<ICISControl[]>) => (this.ciscontrols = res.body || []));
    });
  }

  updateForm(cISResult: ICISResult): void {
    this.editForm.patchValue({
      id: cISResult.id,
      number: cISResult.number,
      description: cISResult.description,
      status: cISResult.status,
      control: cISResult.control,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cISResult = this.createFromForm();
    if (cISResult.id !== undefined) {
      this.subscribeToSaveResponse(this.cISResultService.update(cISResult));
    } else {
      this.subscribeToSaveResponse(this.cISResultService.create(cISResult));
    }
  }

  private createFromForm(): ICISResult {
    return {
      ...new CISResult(),
      id: this.editForm.get(['id'])!.value,
      number: this.editForm.get(['number'])!.value,
      description: this.editForm.get(['description'])!.value,
      status: this.editForm.get(['status'])!.value,
      control: this.editForm.get(['control'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICISResult>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICISControl): any {
    return item.id;
  }
}
