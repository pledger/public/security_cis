import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICISReport } from 'app/shared/model/cis-report.model';

@Component({
  selector: 'jhi-cis-report-detail',
  templateUrl: './cis-report-detail.component.html',
})
export class CISReportDetailComponent implements OnInit {
  cISReport: ICISReport | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cISReport }) => (this.cISReport = cISReport));
  }

  previousState(): void {
    window.history.back();
  }
}
