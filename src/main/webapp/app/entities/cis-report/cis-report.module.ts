import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SecurityCisSharedModule } from 'app/shared/shared.module';
import { CISReportComponent } from './cis-report.component';
import { CISReportDetailComponent } from './cis-report-detail.component';
import { CISReportUpdateComponent } from './cis-report-update.component';
import { CISReportDeleteDialogComponent } from './cis-report-delete-dialog.component';
import { cISReportRoute } from './cis-report.route';

@NgModule({
  imports: [SecurityCisSharedModule, RouterModule.forChild(cISReportRoute)],
  declarations: [CISReportComponent, CISReportDetailComponent, CISReportUpdateComponent, CISReportDeleteDialogComponent],
  entryComponents: [CISReportDeleteDialogComponent],
})
export class SecurityCisCISReportModule {}
