import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICISReport, CISReport } from 'app/shared/model/cis-report.model';
import { CISReportService } from './cis-report.service';
import { ICISReportInfra } from 'app/shared/model/cis-report-infra.model';
import { CISReportInfraService } from 'app/entities/cis-report-infra/cis-report-infra.service';

@Component({
  selector: 'jhi-cis-report-update',
  templateUrl: './cis-report-update.component.html',
})
export class CISReportUpdateComponent implements OnInit {
  isSaving = false;
  cisreportinfras: ICISReportInfra[] = [];

  editForm = this.fb.group({
    id: [],
    timestamp: [],
    reportType: [],
    infrastructure: [],
    node: [],
    totalPass: [],
    totalFail: [],
    totalWarn: [],
    totalInfo: [],
    reportInfra: [],
  });

  constructor(
    protected cISReportService: CISReportService,
    protected cISReportInfraService: CISReportInfraService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cISReport }) => {
      if (!cISReport.id) {
        const today = moment().startOf('day');
        cISReport.timestamp = today;
      }

      this.updateForm(cISReport);

      this.cISReportInfraService.query().subscribe((res: HttpResponse<ICISReportInfra[]>) => (this.cisreportinfras = res.body || []));
    });
  }

  updateForm(cISReport: ICISReport): void {
    this.editForm.patchValue({
      id: cISReport.id,
      timestamp: cISReport.timestamp ? cISReport.timestamp.format(DATE_TIME_FORMAT) : null,
      reportType: cISReport.reportType,
      infrastructure: cISReport.infrastructure,
      node: cISReport.node,
      totalPass: cISReport.totalPass,
      totalFail: cISReport.totalFail,
      totalWarn: cISReport.totalWarn,
      totalInfo: cISReport.totalInfo,
      reportInfra: cISReport.reportInfra,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cISReport = this.createFromForm();
    if (cISReport.id !== undefined) {
      this.subscribeToSaveResponse(this.cISReportService.update(cISReport));
    } else {
      this.subscribeToSaveResponse(this.cISReportService.create(cISReport));
    }
  }

  private createFromForm(): ICISReport {
    return {
      ...new CISReport(),
      id: this.editForm.get(['id'])!.value,
      timestamp: this.editForm.get(['timestamp'])!.value ? moment(this.editForm.get(['timestamp'])!.value, DATE_TIME_FORMAT) : undefined,
      reportType: this.editForm.get(['reportType'])!.value,
      infrastructure: this.editForm.get(['infrastructure'])!.value,
      node: this.editForm.get(['node'])!.value,
      totalPass: this.editForm.get(['totalPass'])!.value,
      totalFail: this.editForm.get(['totalFail'])!.value,
      totalWarn: this.editForm.get(['totalWarn'])!.value,
      totalInfo: this.editForm.get(['totalInfo'])!.value,
      reportInfra: this.editForm.get(['reportInfra'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICISReport>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: ICISReportInfra): any {
    return item.id;
  }
}
