import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICISReport } from 'app/shared/model/cis-report.model';

type EntityResponseType = HttpResponse<ICISReport>;
type EntityArrayResponseType = HttpResponse<ICISReport[]>;

@Injectable({ providedIn: 'root' })
export class CISReportService {
  public resourceUrl = SERVER_API_URL + 'api/cis-reports';

  constructor(protected http: HttpClient) {}

  create(cISReport: ICISReport): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cISReport);
    return this.http
      .post<ICISReport>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(cISReport: ICISReport): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(cISReport);
    return this.http
      .put<ICISReport>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICISReport>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICISReport[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(cISReport: ICISReport): ICISReport {
    const copy: ICISReport = Object.assign({}, cISReport, {
      timestamp: cISReport.timestamp && cISReport.timestamp.isValid() ? cISReport.timestamp.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.timestamp = res.body.timestamp ? moment(res.body.timestamp) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((cISReport: ICISReport) => {
        cISReport.timestamp = cISReport.timestamp ? moment(cISReport.timestamp) : undefined;
      });
    }
    return res;
  }
}
