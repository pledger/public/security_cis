import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICISReport } from 'app/shared/model/cis-report.model';
import { CISReportService } from './cis-report.service';

@Component({
  templateUrl: './cis-report-delete-dialog.component.html',
})
export class CISReportDeleteDialogComponent {
  cISReport?: ICISReport;

  constructor(protected cISReportService: CISReportService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.cISReportService.delete(id).subscribe(() => {
      this.eventManager.broadcast('cISReportListModification');
      this.activeModal.close();
    });
  }
}
