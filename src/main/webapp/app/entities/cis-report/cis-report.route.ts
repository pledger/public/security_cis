import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICISReport, CISReport } from 'app/shared/model/cis-report.model';
import { CISReportService } from './cis-report.service';
import { CISReportComponent } from './cis-report.component';
import { CISReportDetailComponent } from './cis-report-detail.component';
import { CISReportUpdateComponent } from './cis-report-update.component';

@Injectable({ providedIn: 'root' })
export class CISReportResolve implements Resolve<ICISReport> {
  constructor(private service: CISReportService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICISReport> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((cISReport: HttpResponse<CISReport>) => {
          if (cISReport.body) {
            return of(cISReport.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CISReport());
  }
}

export const cISReportRoute: Routes = [
  {
    path: '',
    component: CISReportComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'CISReports',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CISReportDetailComponent,
    resolve: {
      cISReport: CISReportResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISReports',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CISReportUpdateComponent,
    resolve: {
      cISReport: CISReportResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISReports',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CISReportUpdateComponent,
    resolve: {
      cISReport: CISReportResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISReports',
    },
    canActivate: [UserRouteAccessService],
  },
];
