import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'cis-report',
        loadChildren: () => import('./cis-report/cis-report.module').then(m => m.SecurityCisCISReportModule),
      },
      {
        path: 'cis-control',
        loadChildren: () => import('./cis-control/cis-control.module').then(m => m.SecurityCisCISControlModule),
      },
      {
        path: 'cis-result',
        loadChildren: () => import('./cis-result/cis-result.module').then(m => m.SecurityCisCISResultModule),
      },
      {
        path: 'cis-report-infra',
        loadChildren: () => import('./cis-report-infra/cis-report-infra.module').then(m => m.SecurityCisCISReportInfraModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class SecurityCisEntityModule {}
