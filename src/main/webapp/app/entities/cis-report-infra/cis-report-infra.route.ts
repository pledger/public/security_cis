import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICISReportInfra, CISReportInfra } from 'app/shared/model/cis-report-infra.model';
import { CISReportInfraService } from './cis-report-infra.service';
import { CISReportInfraComponent } from './cis-report-infra.component';
import { CISReportInfraDetailComponent } from './cis-report-infra-detail.component';
import { CISReportInfraUpdateComponent } from './cis-report-infra-update.component';

@Injectable({ providedIn: 'root' })
export class CISReportInfraResolve implements Resolve<ICISReportInfra> {
  constructor(private service: CISReportInfraService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICISReportInfra> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((cISReportInfra: HttpResponse<CISReportInfra>) => {
          if (cISReportInfra.body) {
            return of(cISReportInfra.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CISReportInfra());
  }
}

export const cISReportInfraRoute: Routes = [
  {
    path: '',
    component: CISReportInfraComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'CISReportInfras',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CISReportInfraDetailComponent,
    resolve: {
      cISReportInfra: CISReportInfraResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISReportInfras',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CISReportInfraUpdateComponent,
    resolve: {
      cISReportInfra: CISReportInfraResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISReportInfras',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CISReportInfraUpdateComponent,
    resolve: {
      cISReportInfra: CISReportInfraResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'CISReportInfras',
    },
    canActivate: [UserRouteAccessService],
  },
];
