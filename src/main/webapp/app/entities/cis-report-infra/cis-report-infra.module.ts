import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SecurityCisSharedModule } from 'app/shared/shared.module';
import { CISReportInfraComponent } from './cis-report-infra.component';
import { CISReportInfraDetailComponent } from './cis-report-infra-detail.component';
import { CISReportInfraUpdateComponent } from './cis-report-infra-update.component';
import { CISReportInfraDeleteDialogComponent } from './cis-report-infra-delete-dialog.component';
import { cISReportInfraRoute } from './cis-report-infra.route';

@NgModule({
  imports: [SecurityCisSharedModule, RouterModule.forChild(cISReportInfraRoute)],
  declarations: [
    CISReportInfraComponent,
    CISReportInfraDetailComponent,
    CISReportInfraUpdateComponent,
    CISReportInfraDeleteDialogComponent,
  ],
  entryComponents: [CISReportInfraDeleteDialogComponent],
})
export class SecurityCisCISReportInfraModule {}
