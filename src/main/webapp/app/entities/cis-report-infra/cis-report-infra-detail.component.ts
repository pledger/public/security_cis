import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICISReportInfra } from 'app/shared/model/cis-report-infra.model';

@Component({
  selector: 'jhi-cis-report-infra-detail',
  templateUrl: './cis-report-infra-detail.component.html',
})
export class CISReportInfraDetailComponent implements OnInit {
  cISReportInfra: ICISReportInfra | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cISReportInfra }) => (this.cISReportInfra = cISReportInfra));
  }

  previousState(): void {
    window.history.back();
  }
}
