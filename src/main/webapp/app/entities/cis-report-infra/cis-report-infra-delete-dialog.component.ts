import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICISReportInfra } from 'app/shared/model/cis-report-infra.model';
import { CISReportInfraService } from './cis-report-infra.service';

@Component({
  templateUrl: './cis-report-infra-delete-dialog.component.html',
})
export class CISReportInfraDeleteDialogComponent {
  cISReportInfra?: ICISReportInfra;

  constructor(
    protected cISReportInfraService: CISReportInfraService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.cISReportInfraService.delete(id).subscribe(() => {
      this.eventManager.broadcast('cISReportInfraListModification');
      this.activeModal.close();
    });
  }
}
