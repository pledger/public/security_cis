import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { ICISReportInfra } from 'app/shared/model/cis-report-infra.model';

type EntityResponseType = HttpResponse<ICISReportInfra>;
type EntityArrayResponseType = HttpResponse<ICISReportInfra[]>;

@Injectable({ providedIn: 'root' })
export class CISReportInfraService {
  public resourceUrl = SERVER_API_URL + 'api/cis-report-infras';

  constructor(protected http: HttpClient) {}

  create(cISReportInfra: ICISReportInfra): Observable<EntityResponseType> {
    return this.http.post<ICISReportInfra>(this.resourceUrl, cISReportInfra, { observe: 'response' });
  }

  update(cISReportInfra: ICISReportInfra): Observable<EntityResponseType> {
    return this.http.put<ICISReportInfra>(this.resourceUrl, cISReportInfra, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICISReportInfra>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICISReportInfra[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
