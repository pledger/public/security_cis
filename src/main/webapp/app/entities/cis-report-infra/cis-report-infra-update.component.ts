import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICISReportInfra, CISReportInfra } from 'app/shared/model/cis-report-infra.model';
import { CISReportInfraService } from './cis-report-infra.service';

@Component({
  selector: 'jhi-cis-report-infra-update',
  templateUrl: './cis-report-infra-update.component.html',
})
export class CISReportInfraUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    infrastructure: [],
    totalPass: [],
    totalFail: [],
    totalWarn: [],
    totalInfo: [],
  });

  constructor(protected cISReportInfraService: CISReportInfraService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ cISReportInfra }) => {
      this.updateForm(cISReportInfra);
    });
  }

  updateForm(cISReportInfra: ICISReportInfra): void {
    this.editForm.patchValue({
      id: cISReportInfra.id,
      infrastructure: cISReportInfra.infrastructure,
      totalPass: cISReportInfra.totalPass,
      totalFail: cISReportInfra.totalFail,
      totalWarn: cISReportInfra.totalWarn,
      totalInfo: cISReportInfra.totalInfo,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const cISReportInfra = this.createFromForm();
    if (cISReportInfra.id !== undefined) {
      this.subscribeToSaveResponse(this.cISReportInfraService.update(cISReportInfra));
    } else {
      this.subscribeToSaveResponse(this.cISReportInfraService.create(cISReportInfra));
    }
  }

  private createFromForm(): ICISReportInfra {
    return {
      ...new CISReportInfra(),
      id: this.editForm.get(['id'])!.value,
      infrastructure: this.editForm.get(['infrastructure'])!.value,
      totalPass: this.editForm.get(['totalPass'])!.value,
      totalFail: this.editForm.get(['totalFail'])!.value,
      totalWarn: this.editForm.get(['totalWarn'])!.value,
      totalInfo: this.editForm.get(['totalInfo'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICISReportInfra>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
