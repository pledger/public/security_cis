import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { SecurityCisSharedModule } from 'app/shared/shared.module';
import { SecurityCisCoreModule } from 'app/core/core.module';
import { SecurityCisAppRoutingModule } from './app-routing.module';
import { SecurityCisHomeModule } from './home/home.module';
import { SecurityCisEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    SecurityCisSharedModule,
    SecurityCisCoreModule,
    SecurityCisHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    SecurityCisEntityModule,
    SecurityCisAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class SecurityCisAppModule {}
