import { ICISControl } from 'app/shared/model/cis-control.model';

export interface ICISResult {
  id?: number;
  number?: string;
  description?: string;
  status?: string;
  control?: ICISControl;
}

export class CISResult implements ICISResult {
  constructor(
    public id?: number,
    public number?: string,
    public description?: string,
    public status?: string,
    public control?: ICISControl
  ) {}
}
