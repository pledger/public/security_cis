import { Moment } from 'moment';
import { ICISControl } from 'app/shared/model/cis-control.model';
import { ICISReportInfra } from 'app/shared/model/cis-report-infra.model';

export interface ICISReport {
  id?: number;
  timestamp?: Moment;
  reportType?: string;
  infrastructure?: string;
  node?: string;
  totalPass?: number;
  totalFail?: number;
  totalWarn?: number;
  totalInfo?: number;
  controlSets?: ICISControl[];
  reportInfra?: ICISReportInfra;
}

export class CISReport implements ICISReport {
  constructor(
    public id?: number,
    public timestamp?: Moment,
    public reportType?: string,
    public infrastructure?: string,
    public node?: string,
    public totalPass?: number,
    public totalFail?: number,
    public totalWarn?: number,
    public totalInfo?: number,
    public controlSets?: ICISControl[],
    public reportInfra?: ICISReportInfra
  ) {}
}
