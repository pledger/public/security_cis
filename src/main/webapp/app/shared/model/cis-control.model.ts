import { ICISResult } from 'app/shared/model/cis-result.model';
import { ICISReport } from 'app/shared/model/cis-report.model';

export interface ICISControl {
  id?: number;
  version?: string;
  type?: string;
  description?: string;
  totalPass?: number;
  totalFail?: number;
  totalWarn?: number;
  totalInfo?: number;
  resultSets?: ICISResult[];
  report?: ICISReport;
}

export class CISControl implements ICISControl {
  constructor(
    public id?: number,
    public version?: string,
    public type?: string,
    public description?: string,
    public totalPass?: number,
    public totalFail?: number,
    public totalWarn?: number,
    public totalInfo?: number,
    public resultSets?: ICISResult[],
    public report?: ICISReport
  ) {}
}
