import { ICISReport } from 'app/shared/model/cis-report.model';

export interface ICISReportInfra {
  id?: number;
  infrastructure?: string;
  totalPass?: number;
  totalFail?: number;
  totalWarn?: number;
  totalInfo?: number;
  reportSets?: ICISReport[];
}

export class CISReportInfra implements ICISReportInfra {
  constructor(
    public id?: number,
    public infrastructure?: string,
    public totalPass?: number,
    public totalFail?: number,
    public totalWarn?: number,
    public totalInfo?: number,
    public reportSets?: ICISReport[]
  ) {}
}
