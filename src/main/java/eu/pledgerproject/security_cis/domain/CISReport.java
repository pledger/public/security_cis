package eu.pledgerproject.security_cis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A CISReport.
 */
@Entity
@Table(name = "cis_report")
public class CISReport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "timestamp")
    private Instant timestamp;

    @Column(name = "report_type")
    private String reportType;

    @Column(name = "infrastructure")
    private String infrastructure;

    @Column(name = "node")
    private String node;

    @Column(name = "total_pass")
    private Integer totalPass;

    @Column(name = "total_fail")
    private Integer totalFail;

    @Column(name = "total_warn")
    private Integer totalWarn;

    @Column(name = "total_info")
    private Integer totalInfo;

    @OneToMany(mappedBy = "report")
    private Set<CISControl> controlSets = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "reportSets", allowSetters = true)
    private CISReportInfra reportInfra;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public CISReport timestamp(Instant timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getReportType() {
        return reportType;
    }

    public CISReport reportType(String reportType) {
        this.reportType = reportType;
        return this;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getInfrastructure() {
        return infrastructure;
    }

    public CISReport infrastructure(String infrastructure) {
        this.infrastructure = infrastructure;
        return this;
    }

    public void setInfrastructure(String infrastructure) {
        this.infrastructure = infrastructure;
    }

    public String getNode() {
        return node;
    }

    public CISReport node(String node) {
        this.node = node;
        return this;
    }

    public void setNode(String node) {
        this.node = node;
    }

    public Integer getTotalPass() {
        return totalPass;
    }

    public CISReport totalPass(Integer totalPass) {
        this.totalPass = totalPass;
        return this;
    }

    public void setTotalPass(Integer totalPass) {
        this.totalPass = totalPass;
    }

    public Integer getTotalFail() {
        return totalFail;
    }

    public CISReport totalFail(Integer totalFail) {
        this.totalFail = totalFail;
        return this;
    }

    public void setTotalFail(Integer totalFail) {
        this.totalFail = totalFail;
    }

    public Integer getTotalWarn() {
        return totalWarn;
    }

    public CISReport totalWarn(Integer totalWarn) {
        this.totalWarn = totalWarn;
        return this;
    }

    public void setTotalWarn(Integer totalWarn) {
        this.totalWarn = totalWarn;
    }

    public Integer getTotalInfo() {
        return totalInfo;
    }

    public CISReport totalInfo(Integer totalInfo) {
        this.totalInfo = totalInfo;
        return this;
    }

    public void setTotalInfo(Integer totalInfo) {
        this.totalInfo = totalInfo;
    }

    public Set<CISControl> getControlSets() {
        return controlSets;
    }

    public CISReport controlSets(Set<CISControl> cISControls) {
        this.controlSets = cISControls;
        return this;
    }

    public CISReport addControlSet(CISControl cISControl) {
        this.controlSets.add(cISControl);
        cISControl.setReport(this);
        return this;
    }

    public CISReport removeControlSet(CISControl cISControl) {
        this.controlSets.remove(cISControl);
        cISControl.setReport(null);
        return this;
    }

    public void setControlSets(Set<CISControl> cISControls) {
        this.controlSets = cISControls;
    }

    public CISReportInfra getReportInfra() {
        return reportInfra;
    }

    public CISReport reportInfra(CISReportInfra cISReportInfra) {
        this.reportInfra = cISReportInfra;
        return this;
    }

    public void setReportInfra(CISReportInfra cISReportInfra) {
        this.reportInfra = cISReportInfra;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CISReport)) {
            return false;
        }
        return id != null && id.equals(((CISReport) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CISReport{" +
            "id=" + getId() +
            ", timestamp='" + getTimestamp() + "'" +
            ", reportType='" + getReportType() + "'" +
            ", infrastructure='" + getInfrastructure() + "'" +
            ", node='" + getNode() + "'" +
            ", totalPass=" + getTotalPass() +
            ", totalFail=" + getTotalFail() +
            ", totalWarn=" + getTotalWarn() +
            ", totalInfo=" + getTotalInfo() +
            "}";
    }
}
