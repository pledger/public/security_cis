package eu.pledgerproject.security_cis.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A CISControl.
 */
@Entity
@Table(name = "cis_control")
public class CISControl implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "version")
    private String version;

    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String description;

    @Column(name = "total_pass")
    private Integer totalPass;

    @Column(name = "total_fail")
    private Integer totalFail;

    @Column(name = "total_warn")
    private Integer totalWarn;

    @Column(name = "total_info")
    private Integer totalInfo;

    @OneToMany(mappedBy = "control", cascade = CascadeType.ALL)
    private Set<CISResult> resultSets = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "controlSets", allowSetters = true)
    private CISReport report;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public CISControl version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public CISControl type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public CISControl description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTotalPass() {
        return totalPass;
    }

    public CISControl totalPass(Integer totalPass) {
        this.totalPass = totalPass;
        return this;
    }

    public void setTotalPass(Integer totalPass) {
        this.totalPass = totalPass;
    }

    public Integer getTotalFail() {
        return totalFail;
    }

    public CISControl totalFail(Integer totalFail) {
        this.totalFail = totalFail;
        return this;
    }

    public void setTotalFail(Integer totalFail) {
        this.totalFail = totalFail;
    }

    public Integer getTotalWarn() {
        return totalWarn;
    }

    public CISControl totalWarn(Integer totalWarn) {
        this.totalWarn = totalWarn;
        return this;
    }

    public void setTotalWarn(Integer totalWarn) {
        this.totalWarn = totalWarn;
    }

    public Integer getTotalInfo() {
        return totalInfo;
    }

    public CISControl totalInfo(Integer totalInfo) {
        this.totalInfo = totalInfo;
        return this;
    }

    public void setTotalInfo(Integer totalInfo) {
        this.totalInfo = totalInfo;
    }

    public Set<CISResult> getResultSets() {
        return resultSets;
    }

    public CISControl resultSets(Set<CISResult> cISResults) {
        this.resultSets = cISResults;
        return this;
    }

    public CISControl addResultSet(CISResult cISResult) {
        this.resultSets.add(cISResult);
        cISResult.setControl(this);
        return this;
    }

    public CISControl removeResultSet(CISResult cISResult) {
        this.resultSets.remove(cISResult);
        cISResult.setControl(null);
        return this;
    }

    public void setResultSets(Set<CISResult> cISResults) {
        this.resultSets = cISResults;
    }

    public CISReport getReport() {
        return report;
    }

    public CISControl report(CISReport cISReport) {
        this.report = cISReport;
        return this;
    }

    public void setReport(CISReport cISReport) {
        this.report = cISReport;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CISControl)) {
            return false;
        }
        return id != null && id.equals(((CISControl) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CISControl{" +
            "id=" + getId() +
            ", version='" + getVersion() + "'" +
            ", type='" + getType() + "'" +
            ", description='" + getDescription() + "'" +
            ", totalPass=" + getTotalPass() +
            ", totalFail=" + getTotalFail() +
            ", totalWarn=" + getTotalWarn() +
            ", totalInfo=" + getTotalInfo() +
            "}";
    }
}
