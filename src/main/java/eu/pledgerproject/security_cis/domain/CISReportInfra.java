package eu.pledgerproject.security_cis.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A CISReportInfra.
 */
@Entity
@Table(name = "cis_report_infra")
public class CISReportInfra implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "infrastructure")
    private String infrastructure;

    @Column(name = "total_pass")
    private Integer totalPass;

    @Column(name = "total_fail")
    private Integer totalFail;

    @Column(name = "total_warn")
    private Integer totalWarn;

    @Column(name = "total_info")
    private Integer totalInfo;

    @OneToMany(mappedBy = "reportInfra")
    private Set<CISReport> reportSets = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInfrastructure() {
        return infrastructure;
    }

    public CISReportInfra infrastructure(String infrastructure) {
        this.infrastructure = infrastructure;
        return this;
    }

    public void setInfrastructure(String infrastructure) {
        this.infrastructure = infrastructure;
    }

    public Integer getTotalPass() {
        return totalPass;
    }

    public CISReportInfra totalPass(Integer totalPass) {
        this.totalPass = totalPass;
        return this;
    }

    public void setTotalPass(Integer totalPass) {
        this.totalPass = totalPass;
    }

    public Integer getTotalFail() {
        return totalFail;
    }

    public CISReportInfra totalFail(Integer totalFail) {
        this.totalFail = totalFail;
        return this;
    }

    public void setTotalFail(Integer totalFail) {
        this.totalFail = totalFail;
    }

    public Integer getTotalWarn() {
        return totalWarn;
    }

    public CISReportInfra totalWarn(Integer totalWarn) {
        this.totalWarn = totalWarn;
        return this;
    }

    public void setTotalWarn(Integer totalWarn) {
        this.totalWarn = totalWarn;
    }

    public Integer getTotalInfo() {
        return totalInfo;
    }

    public CISReportInfra totalInfo(Integer totalInfo) {
        this.totalInfo = totalInfo;
        return this;
    }

    public void setTotalInfo(Integer totalInfo) {
        this.totalInfo = totalInfo;
    }

    public Set<CISReport> getReportSets() {
        return reportSets;
    }

    public CISReportInfra reportSets(Set<CISReport> cISReports) {
        this.reportSets = cISReports;
        return this;
    }

    public CISReportInfra addReportSet(CISReport cISReport) {
        this.reportSets.add(cISReport);
        cISReport.setReportInfra(this);
        return this;
    }

    public CISReportInfra removeReportSet(CISReport cISReport) {
        this.reportSets.remove(cISReport);
        cISReport.setReportInfra(null);
        return this;
    }

    public void setReportSets(Set<CISReport> cISReports) {
        this.reportSets = cISReports;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CISReportInfra)) {
            return false;
        }
        return id != null && id.equals(((CISReportInfra) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CISReportInfra{" +
            "id=" + getId() +
            ", infrastructure='" + getInfrastructure() + "'" +
            ", totalPass=" + getTotalPass() +
            ", totalFail=" + getTotalFail() +
            ", totalWarn=" + getTotalWarn() +
            ", totalInfo=" + getTotalInfo() +
            "}";
    }
}
