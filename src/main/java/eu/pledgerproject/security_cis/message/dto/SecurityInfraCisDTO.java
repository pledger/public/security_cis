package eu.pledgerproject.security_cis.message.dto;

import eu.pledgerproject.security_cis.domain.CISReport;

public class SecurityInfraCisDTO {

	private CISReport cisReport;
	public SecurityInfraCisDTO() {
		
	}
	public CISReport getCisReport() {
		return cisReport;
	}
	public void setCisReport(CISReport cisReport) {
		this.cisReport = cisReport;
	}
	
}
