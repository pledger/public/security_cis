package eu.pledgerproject.security_cis.message;

import org.apache.kafka.common.serialization.Deserializer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.pledgerproject.security_cis.domain.CISControl;
import eu.pledgerproject.security_cis.domain.CISReport;
import eu.pledgerproject.security_cis.domain.CISResult;
import eu.pledgerproject.security_cis.message.dto.SecurityInfraCisDTO;

public class DeserializerSecurityInfraCisDTO implements Deserializer<SecurityInfraCisDTO> {
    private static final Logger log = LoggerFactory.getLogger(DeserializerSecurityInfraCisDTO.class);


	@Override
	public SecurityInfraCisDTO deserialize(String topic, byte[] data) {
		
		String source = new String(data);
		log.info("received: " + source);
		
		CISReport cisReport = new CISReport();

		JSONObject rootJSON = new JSONObject(source);
		JSONObject totalsJSON = rootJSON.getJSONObject("Totals");
		cisReport.setTotalFail(totalsJSON.getInt("total_fail"));
		cisReport.setTotalPass(totalsJSON.getInt("total_pass"));
		cisReport.setTotalWarn(totalsJSON.getInt("total_warn"));
		cisReport.setTotalInfo(totalsJSON.getInt("total_info"));
		
		cisReport.setNode(rootJSON.getString("hostname"));

		JSONArray controlsJSON = rootJSON.getJSONArray("Controls");
		for(int i=0; i<controlsJSON.length(); i++) {
			JSONObject controlJSON = controlsJSON.getJSONObject(i);
			CISControl cisControl = new CISControl();
			cisControl.setReport(cisReport);
			cisControl.setType(controlJSON.getString("node_type"));
			cisControl.setDescription(controlJSON.getString("text"));
			cisControl.setVersion(controlJSON.getString("version"));
			cisControl.setTotalFail(controlJSON.getInt("total_fail"));
			cisControl.setTotalPass(controlJSON.getInt("total_pass"));
			cisControl.setTotalWarn(controlJSON.getInt("total_warn"));
			cisControl.setTotalInfo(controlJSON.getInt("total_info"));
			cisReport.getControlSets().add(cisControl);
			JSONArray testsJSON = controlJSON.getJSONArray("tests");
			for(int j=0; j<testsJSON.length(); j++) {
				JSONObject testJSON = testsJSON.getJSONObject(j);
				JSONArray resultsJSON = testJSON.getJSONArray("results");
				for(int k=0; k<resultsJSON.length(); k++) {
					JSONObject resultJSON = resultsJSON.getJSONObject(k);
					CISResult cisResult = new CISResult();
					cisResult.setControl(cisControl);
					cisResult.setNumber(resultJSON.getString("test_number"));
					cisResult.setStatus(resultJSON.getString("status"));
					cisResult.setDescription(resultJSON.getString("test_desc"));
					cisControl.getResultSets().add(cisResult);
				}
			}
		}
		
		SecurityInfraCisDTO result = new SecurityInfraCisDTO();
		result.setCisReport(cisReport);
		
		return result;
	}
    
	
}

