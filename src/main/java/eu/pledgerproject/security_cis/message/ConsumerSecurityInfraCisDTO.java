package eu.pledgerproject.security_cis.message;

import java.time.Instant;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import eu.pledgerproject.security_cis.domain.CISControl;
import eu.pledgerproject.security_cis.domain.CISReportInfra;
import eu.pledgerproject.security_cis.domain.CISResult;
import eu.pledgerproject.security_cis.message.dto.SecurityInfraCisDTO;
import eu.pledgerproject.security_cis.repository.CISControlRepository;
import eu.pledgerproject.security_cis.repository.CISReportInfraRepository;
import eu.pledgerproject.security_cis.repository.CISReportRepository;
import eu.pledgerproject.security_cis.repository.CISResultRepository;

@org.springframework.stereotype.Service
public class ConsumerSecurityInfraCisDTO { 
	private static final Logger log = LoggerFactory.getLogger(ConsumerSecurityInfraCisDTO.class);
	@Value("${CONFSERVICE_URL}")
	private String confServiceURL;

	@Value("${CONFSERVICE_USER}")
	private String confServiceUser;

	@Value("${CONFSERVICE_PASS}")
	private String confServicePass;

	
	private final CISReportRepository cisReportRepository;
	private final CISReportInfraRepository cisReportInfraRepository;
	private final CISControlRepository cisControlRepository;
	private final CISResultRepository cisResultRepository;
	

	public ConsumerSecurityInfraCisDTO(CISReportRepository cisReportRepository, CISReportInfraRepository cisReportInfraRepository, CISControlRepository cisControlRepository, CISResultRepository cisResultRepository) {
		this.cisReportRepository = cisReportRepository;
		this.cisReportInfraRepository = cisReportInfraRepository;
		this.cisControlRepository = cisControlRepository;
		this.cisResultRepository = cisResultRepository;
	}
	
	@KafkaListener(topics = "security_infra_cis", groupId = "id", containerFactory = "securityInfraCisDTOListener") 
	public void consume(SecurityInfraCisDTO message) { 
		log.info("New SecurityInfraCisDTO received: " + message); 

		String[] infrastructureData = getInfrastructureData(message.getCisReport().getNode());
		String infrastructure = infrastructureData[0];
		String reportType = infrastructureData[1];
		Instant timestamp = Instant.now();
		
		message.getCisReport().setInfrastructure(infrastructure);
		message.getCisReport().setReportType(reportType);
		message.getCisReport().setTimestamp(timestamp);
		
		int totalInfo = message.getCisReport().getTotalInfo();
		int totalWarn = message.getCisReport().getTotalWarn();
		int totalFail = message.getCisReport().getTotalFail();
		int totalPass = message.getCisReport().getTotalPass();

		Optional<CISReportInfra> cisReportInfraDB = cisReportInfraRepository.findByInfrastructure(infrastructure);
		if(cisReportInfraDB.isPresent()) {
			cisReportInfraDB.get().setInfrastructure(infrastructure);
			cisReportInfraDB.get().setTotalInfo(cisReportInfraDB.get().getTotalInfo() + totalInfo);
			cisReportInfraDB.get().setTotalWarn(cisReportInfraDB.get().getTotalWarn() + totalWarn);
			cisReportInfraDB.get().setTotalFail(cisReportInfraDB.get().getTotalFail() + totalFail);
			cisReportInfraDB.get().setTotalPass(cisReportInfraDB.get().getTotalPass() + totalPass);
			message.getCisReport().setReportInfra(cisReportInfraDB.get());
			cisReportInfraRepository.save(cisReportInfraDB.get());
		}
		else {
			CISReportInfra cisReportInfra = new CISReportInfra();
			cisReportInfra.setInfrastructure(infrastructure);
			cisReportInfra.setTotalInfo(totalInfo);
			cisReportInfra.setTotalWarn(totalWarn);
			cisReportInfra.setTotalFail(totalFail);
			cisReportInfra.setTotalPass(totalPass);
			cisReportInfraRepository.save(cisReportInfra);
			
			message.getCisReport().setReportInfra(cisReportInfra);
		}
		
		cisReportRepository.save(message.getCisReport());
		for(CISControl cisControl : message.getCisReport().getControlSets()) {
			cisControl.setReport(message.getCisReport());
			cisControlRepository.save(cisControl);
			for(CISResult cisResult : cisControl.getResultSets()) {
				cisResult.setControl(cisControl);
				cisResultRepository.save(cisResult);
			}
		}

	}

	private String[] getInfrastructureData(String hostname) {
		String[] result = new String[] {"", ""};
		
		if(hostname != null) {
			try {
				Unirest.setTimeouts(0, 0);
				HttpResponse<String> responseLogin = Unirest.post(confServiceURL + "/api/authenticate")
						.header("Content-Type", "application/json")
						.body("{\"username\": \""+confServiceUser+"\",\"password\": \""+confServicePass+"\"}")
						.asString();
				String token = new JSONObject(responseLogin.getBody()).getString("id_token");
	
				HttpResponse<String> responseNode = Unirest.get(confServiceURL + "/api/nodes")
						.header("Authorization", "Bearer " + token)
						.asString();
				JSONArray nodes = new JSONArray(responseNode.getBody());
				for(int i =0; i<nodes.length(); i++) {
					JSONObject node = nodes.getJSONObject(i);
					if(hostname.equals(node.getString("name"))){
						JSONObject infrastructure = node.getJSONObject("infrastructure");
						result[0] = infrastructure.getString("name");
						result[1] = infrastructure.getString("type");
					}
				}
				
			} catch (UnirestException e) {
				log.error("Http error", e);
			}
		}
		return result;

	}

} 