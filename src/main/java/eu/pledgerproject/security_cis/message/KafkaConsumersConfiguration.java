package eu.pledgerproject.security_cis.message;

import java.util.Map;

import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.SeekToCurrentErrorHandler;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer2;

import eu.pledgerproject.security_cis.config.KafkaProperties;
import eu.pledgerproject.security_cis.message.dto.SecurityInfraCisDTO;

@EnableKafka
@Configuration
public class KafkaConsumersConfiguration { 
	
	private KafkaProperties kafkaProperties;
	
	public KafkaConsumersConfiguration(KafkaProperties kafkaProperties) {
		this.kafkaProperties = kafkaProperties;
	}
	
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Bean
    public ConsumerFactory<String, SecurityInfraCisDTO> securityInfraCisDTOConsumer() { 
        Map<String, Object> map = kafkaProperties.getConsumerProps();
        return new DefaultKafkaConsumerFactory<String, SecurityInfraCisDTO>(map, new StringDeserializer(), new ErrorHandlingDeserializer2(new DeserializerSecurityInfraCisDTO())); 
    } 
  
    @SuppressWarnings({ "deprecation" })
	@Bean
    public ConcurrentKafkaListenerContainerFactory<String, SecurityInfraCisDTO> securityInfraCisDTOListener() { 
        ConcurrentKafkaListenerContainerFactory<String, SecurityInfraCisDTO> factory = new ConcurrentKafkaListenerContainerFactory<>(); 
        factory.setConsumerFactory(securityInfraCisDTOConsumer()); 
        factory.setErrorHandler(new SeekToCurrentErrorHandler(1));
        return factory; 
    }

}