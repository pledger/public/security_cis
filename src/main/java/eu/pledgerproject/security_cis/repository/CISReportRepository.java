package eu.pledgerproject.security_cis.repository;

import eu.pledgerproject.security_cis.domain.CISReport;
import eu.pledgerproject.security_cis.domain.CISReportInfra;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CISReport entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CISReportRepository extends JpaRepository<CISReport, Long> {
	
	@Query(value = "select cisReport from CISReport cisReport order by cisReport.timestamp desc ")
	List<CISReport> findAll();
	
}
