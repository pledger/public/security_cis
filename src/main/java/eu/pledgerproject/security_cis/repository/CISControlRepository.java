package eu.pledgerproject.security_cis.repository;

import eu.pledgerproject.security_cis.domain.CISControl;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CISControl entity.
 */
@Repository
public interface CISControlRepository extends JpaRepository<CISControl, Long> {
}
