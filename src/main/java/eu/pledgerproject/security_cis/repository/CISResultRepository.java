package eu.pledgerproject.security_cis.repository;

import eu.pledgerproject.security_cis.domain.CISResult;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CISResult entity.
 */
@Repository
public interface CISResultRepository extends JpaRepository<CISResult, Long> {
}
