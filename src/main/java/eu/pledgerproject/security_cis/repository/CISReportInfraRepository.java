package eu.pledgerproject.security_cis.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import eu.pledgerproject.security_cis.domain.CISControl;
import eu.pledgerproject.security_cis.domain.CISReportInfra;

/**
 * Spring Data  repository for the CISReportInfra entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CISReportInfraRepository extends JpaRepository<CISReportInfra, Long> {
	
	@Query(value = "select cisReportInfra from CISReportInfra cisReportInfra where cisReportInfra.infrastructure = :infrastructure order by infrastructure ")
	Optional<CISReportInfra> findByInfrastructure(@Param("infrastructure") String infrastructure);
	
}
