package eu.pledgerproject.security_cis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import eu.pledgerproject.security_cis.domain.CISReportInfra;
import eu.pledgerproject.security_cis.service.CISReportInfraService;
import eu.pledgerproject.security_cis.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.pledgerproject.security_cis.domain.CISReportInfra}.
 */
@RestController
@RequestMapping("/api")
public class CISReportInfraResource {

    private final Logger log = LoggerFactory.getLogger(CISReportInfraResource.class);

    private static final String ENTITY_NAME = "cISReportInfra";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CISReportInfraService cISReportInfraService;

    public CISReportInfraResource(CISReportInfraService cISReportInfraService) {
        this.cISReportInfraService = cISReportInfraService;
    }

    /**
     * {@code POST  /cis-report-infras} : Create a new cISReportInfra.
     *
     * @param cISReportInfra the cISReportInfra to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cISReportInfra, or with status {@code 400 (Bad Request)} if the cISReportInfra has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cis-report-infras")
    public ResponseEntity<CISReportInfra> createCISReportInfra(@RequestBody CISReportInfra cISReportInfra) throws URISyntaxException {
        log.debug("REST request to save CISReportInfra : {}", cISReportInfra);
        if (cISReportInfra.getId() != null) {
            throw new BadRequestAlertException("A new cISReportInfra cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CISReportInfra result = cISReportInfraService.save(cISReportInfra);
        return ResponseEntity.created(new URI("/api/cis-report-infras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cis-report-infras} : Updates an existing cISReportInfra.
     *
     * @param cISReportInfra the cISReportInfra to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cISReportInfra,
     * or with status {@code 400 (Bad Request)} if the cISReportInfra is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cISReportInfra couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cis-report-infras")
    public ResponseEntity<CISReportInfra> updateCISReportInfra(@RequestBody CISReportInfra cISReportInfra) throws URISyntaxException {
        log.debug("REST request to update CISReportInfra : {}", cISReportInfra);
        if (cISReportInfra.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CISReportInfra result = cISReportInfraService.save(cISReportInfra);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cISReportInfra.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cis-report-infras} : get all the cISReportInfras.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cISReportInfras in body.
     */
    @GetMapping("/cis-report-infras")
    public ResponseEntity<List<CISReportInfra>> getAllCISReportInfras(Pageable pageable) {
        log.debug("REST request to get a page of CISReportInfras");
        Page<CISReportInfra> page = cISReportInfraService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cis-report-infras/:id} : get the "id" cISReportInfra.
     *
     * @param id the id of the cISReportInfra to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cISReportInfra, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cis-report-infras/{id}")
    public ResponseEntity<CISReportInfra> getCISReportInfra(@PathVariable Long id) {
        log.debug("REST request to get CISReportInfra : {}", id);
        Optional<CISReportInfra> cISReportInfra = cISReportInfraService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cISReportInfra);
    }

    /**
     * {@code DELETE  /cis-report-infras/:id} : delete the "id" cISReportInfra.
     *
     * @param id the id of the cISReportInfra to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cis-report-infras/{id}")
    public ResponseEntity<Void> deleteCISReportInfra(@PathVariable Long id) {
        log.debug("REST request to delete CISReportInfra : {}", id);
        cISReportInfraService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
