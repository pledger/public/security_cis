package eu.pledgerproject.security_cis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import eu.pledgerproject.security_cis.domain.CISReport;
import eu.pledgerproject.security_cis.service.CISReportService;
import eu.pledgerproject.security_cis.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.pledgerproject.security_cis.domain.CISReport}.
 */
@RestController
@RequestMapping("/api")
public class CISReportResource {

    private final Logger log = LoggerFactory.getLogger(CISReportResource.class);

    private static final String ENTITY_NAME = "cISReport";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CISReportService cISReportService;

    public CISReportResource(CISReportService cISReportService) {
        this.cISReportService = cISReportService;
    }

    /**
     * {@code POST  /cis-reports} : Create a new cISReport.
     *
     * @param cISReport the cISReport to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cISReport, or with status {@code 400 (Bad Request)} if the cISReport has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cis-reports")
    public ResponseEntity<CISReport> createCISReport(@RequestBody CISReport cISReport) throws URISyntaxException {
        log.debug("REST request to save CISReport : {}", cISReport);
        if (cISReport.getId() != null) {
            throw new BadRequestAlertException("A new cISReport cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CISReport result = cISReportService.save(cISReport);
        return ResponseEntity.created(new URI("/api/cis-reports/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cis-reports} : Updates an existing cISReport.
     *
     * @param cISReport the cISReport to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cISReport,
     * or with status {@code 400 (Bad Request)} if the cISReport is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cISReport couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cis-reports")
    public ResponseEntity<CISReport> updateCISReport(@RequestBody CISReport cISReport) throws URISyntaxException {
        log.debug("REST request to update CISReport : {}", cISReport);
        if (cISReport.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CISReport result = cISReportService.save(cISReport);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cISReport.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cis-reports} : get all the cISReports.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cISReports in body.
     */
    @GetMapping("/cis-reports")
    public ResponseEntity<List<CISReport>> getAllCISReports(Pageable pageable) {
        log.debug("REST request to get a page of CISReports");
        Page<CISReport> page = cISReportService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cis-reports/:id} : get the "id" cISReport.
     *
     * @param id the id of the cISReport to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cISReport, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cis-reports/{id}")
    public ResponseEntity<CISReport> getCISReport(@PathVariable Long id) {
        log.debug("REST request to get CISReport : {}", id);
        Optional<CISReport> cISReport = cISReportService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cISReport);
    }

    /**
     * {@code DELETE  /cis-reports/:id} : delete the "id" cISReport.
     *
     * @param id the id of the cISReport to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cis-reports/{id}")
    public ResponseEntity<Void> deleteCISReport(@PathVariable Long id) {
        log.debug("REST request to delete CISReport : {}", id);
        cISReportService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
