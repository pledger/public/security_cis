package eu.pledgerproject.security_cis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import eu.pledgerproject.security_cis.domain.CISControl;
import eu.pledgerproject.security_cis.service.CISControlService;
import eu.pledgerproject.security_cis.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.pledgerproject.security_cis.domain.CISControl}.
 */
@RestController
@RequestMapping("/api")
public class CISControlResource {

    private final Logger log = LoggerFactory.getLogger(CISControlResource.class);

    private static final String ENTITY_NAME = "cISControl";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CISControlService cISControlService;

    public CISControlResource(CISControlService cISControlService) {
        this.cISControlService = cISControlService;
    }

    /**
     * {@code POST  /cis-controls} : Create a new cISControl.
     *
     * @param cISControl the cISControl to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cISControl, or with status {@code 400 (Bad Request)} if the cISControl has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cis-controls")
    public ResponseEntity<CISControl> createCISControl(@RequestBody CISControl cISControl) throws URISyntaxException {
        log.debug("REST request to save CISControl : {}", cISControl);
        if (cISControl.getId() != null) {
            throw new BadRequestAlertException("A new cISControl cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CISControl result = cISControlService.save(cISControl);
        return ResponseEntity.created(new URI("/api/cis-controls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cis-controls} : Updates an existing cISControl.
     *
     * @param cISControl the cISControl to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cISControl,
     * or with status {@code 400 (Bad Request)} if the cISControl is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cISControl couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cis-controls")
    public ResponseEntity<CISControl> updateCISControl(@RequestBody CISControl cISControl) throws URISyntaxException {
        log.debug("REST request to update CISControl : {}", cISControl);
        if (cISControl.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CISControl result = cISControlService.save(cISControl);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cISControl.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cis-controls} : get all the cISControls.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cISControls in body.
     */
    @GetMapping("/cis-controls")
    public ResponseEntity<List<CISControl>> getAllCISControls(Pageable pageable) {
        log.debug("REST request to get a page of CISControls");
        Page<CISControl> page = cISControlService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cis-controls/:id} : get the "id" cISControl.
     *
     * @param id the id of the cISControl to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cISControl, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cis-controls/{id}")
    public ResponseEntity<CISControl> getCISControl(@PathVariable Long id) {
        log.debug("REST request to get CISControl : {}", id);
        Optional<CISControl> cISControl = cISControlService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cISControl);
    }

    /**
     * {@code DELETE  /cis-controls/:id} : delete the "id" cISControl.
     *
     * @param id the id of the cISControl to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cis-controls/{id}")
    public ResponseEntity<Void> deleteCISControl(@PathVariable Long id) {
        log.debug("REST request to delete CISControl : {}", id);
        cISControlService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
