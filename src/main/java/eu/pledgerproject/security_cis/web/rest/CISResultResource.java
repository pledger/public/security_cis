package eu.pledgerproject.security_cis.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import eu.pledgerproject.security_cis.domain.CISResult;
import eu.pledgerproject.security_cis.service.CISResultService;
import eu.pledgerproject.security_cis.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link eu.pledgerproject.security_cis.domain.CISResult}.
 */
@RestController
@RequestMapping("/api")
public class CISResultResource {

    private final Logger log = LoggerFactory.getLogger(CISResultResource.class);

    private static final String ENTITY_NAME = "cISResult";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CISResultService cISResultService;

    public CISResultResource(CISResultService cISResultService) {
        this.cISResultService = cISResultService;
    }

    /**
     * {@code POST  /cis-results} : Create a new cISResult.
     *
     * @param cISResult the cISResult to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cISResult, or with status {@code 400 (Bad Request)} if the cISResult has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cis-results")
    public ResponseEntity<CISResult> createCISResult(@RequestBody CISResult cISResult) throws URISyntaxException {
        log.debug("REST request to save CISResult : {}", cISResult);
        if (cISResult.getId() != null) {
            throw new BadRequestAlertException("A new cISResult cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CISResult result = cISResultService.save(cISResult);
        return ResponseEntity.created(new URI("/api/cis-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cis-results} : Updates an existing cISResult.
     *
     * @param cISResult the cISResult to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cISResult,
     * or with status {@code 400 (Bad Request)} if the cISResult is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cISResult couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cis-results")
    public ResponseEntity<CISResult> updateCISResult(@RequestBody CISResult cISResult) throws URISyntaxException {
        log.debug("REST request to update CISResult : {}", cISResult);
        if (cISResult.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CISResult result = cISResultService.save(cISResult);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cISResult.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cis-results} : get all the cISResults.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cISResults in body.
     */
    @GetMapping("/cis-results")
    public ResponseEntity<List<CISResult>> getAllCISResults(Pageable pageable) {
        log.debug("REST request to get a page of CISResults");
        Page<CISResult> page = cISResultService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /cis-results/:id} : get the "id" cISResult.
     *
     * @param id the id of the cISResult to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cISResult, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cis-results/{id}")
    public ResponseEntity<CISResult> getCISResult(@PathVariable Long id) {
        log.debug("REST request to get CISResult : {}", id);
        Optional<CISResult> cISResult = cISResultService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cISResult);
    }

    /**
     * {@code DELETE  /cis-results/:id} : delete the "id" cISResult.
     *
     * @param id the id of the cISResult to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cis-results/{id}")
    public ResponseEntity<Void> deleteCISResult(@PathVariable Long id) {
        log.debug("REST request to delete CISResult : {}", id);
        cISResultService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
