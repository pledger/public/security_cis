/**
 * View Models used by Spring MVC REST controllers.
 */
package eu.pledgerproject.security_cis.web.rest.vm;
