package eu.pledgerproject.security_cis.service;

import eu.pledgerproject.security_cis.domain.CISReportInfra;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link CISReportInfra}.
 */
public interface CISReportInfraService {

    /**
     * Save a cISReportInfra.
     *
     * @param cISReportInfra the entity to save.
     * @return the persisted entity.
     */
    CISReportInfra save(CISReportInfra cISReportInfra);

    /**
     * Get all the cISReportInfras.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CISReportInfra> findAll(Pageable pageable);


    /**
     * Get the "id" cISReportInfra.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CISReportInfra> findOne(Long id);

    /**
     * Delete the "id" cISReportInfra.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
