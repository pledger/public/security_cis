package eu.pledgerproject.security_cis.service;

import eu.pledgerproject.security_cis.domain.CISResult;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link CISResult}.
 */
public interface CISResultService {

    /**
     * Save a cISResult.
     *
     * @param cISResult the entity to save.
     * @return the persisted entity.
     */
    CISResult save(CISResult cISResult);

    /**
     * Get all the cISResults.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CISResult> findAll(Pageable pageable);


    /**
     * Get the "id" cISResult.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CISResult> findOne(Long id);

    /**
     * Delete the "id" cISResult.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
