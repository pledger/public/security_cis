package eu.pledgerproject.security_cis.service;

import eu.pledgerproject.security_cis.domain.CISControl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link CISControl}.
 */
public interface CISControlService {

    /**
     * Save a cISControl.
     *
     * @param cISControl the entity to save.
     * @return the persisted entity.
     */
    CISControl save(CISControl cISControl);

    /**
     * Get all the cISControls.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CISControl> findAll(Pageable pageable);


    /**
     * Get the "id" cISControl.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CISControl> findOne(Long id);

    /**
     * Delete the "id" cISControl.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
