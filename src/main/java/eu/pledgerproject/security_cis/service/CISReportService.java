package eu.pledgerproject.security_cis.service;

import eu.pledgerproject.security_cis.domain.CISReport;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link CISReport}.
 */
public interface CISReportService {

    /**
     * Save a cISReport.
     *
     * @param cISReport the entity to save.
     * @return the persisted entity.
     */
    CISReport save(CISReport cISReport);

    /**
     * Get all the cISReports.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CISReport> findAll(Pageable pageable);


    /**
     * Get the "id" cISReport.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CISReport> findOne(Long id);

    /**
     * Delete the "id" cISReport.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
