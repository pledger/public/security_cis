package eu.pledgerproject.security_cis.service.impl;

import eu.pledgerproject.security_cis.service.CISReportInfraService;
import eu.pledgerproject.security_cis.domain.CISReportInfra;
import eu.pledgerproject.security_cis.repository.CISReportInfraRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CISReportInfra}.
 */
@Service
@Transactional
public class CISReportInfraServiceImpl implements CISReportInfraService {

    private final Logger log = LoggerFactory.getLogger(CISReportInfraServiceImpl.class);

    private final CISReportInfraRepository cISReportInfraRepository;

    public CISReportInfraServiceImpl(CISReportInfraRepository cISReportInfraRepository) {
        this.cISReportInfraRepository = cISReportInfraRepository;
    }

    @Override
    public CISReportInfra save(CISReportInfra cISReportInfra) {
        log.debug("Request to save CISReportInfra : {}", cISReportInfra);
        return cISReportInfraRepository.save(cISReportInfra);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CISReportInfra> findAll(Pageable pageable) {
        log.debug("Request to get all CISReportInfras");
        return cISReportInfraRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CISReportInfra> findOne(Long id) {
        log.debug("Request to get CISReportInfra : {}", id);
        return cISReportInfraRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CISReportInfra : {}", id);
        cISReportInfraRepository.deleteById(id);
    }
}
