package eu.pledgerproject.security_cis.service.impl;

import eu.pledgerproject.security_cis.service.CISControlService;
import eu.pledgerproject.security_cis.domain.CISControl;
import eu.pledgerproject.security_cis.repository.CISControlRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CISControl}.
 */
@Service
@Transactional
public class CISControlServiceImpl implements CISControlService {

    private final Logger log = LoggerFactory.getLogger(CISControlServiceImpl.class);

    private final CISControlRepository cISControlRepository;

    public CISControlServiceImpl(CISControlRepository cISControlRepository) {
        this.cISControlRepository = cISControlRepository;
    }

    @Override
    public CISControl save(CISControl cISControl) {
        log.debug("Request to save CISControl : {}", cISControl);
        return cISControlRepository.save(cISControl);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CISControl> findAll(Pageable pageable) {
        log.debug("Request to get all CISControls");
        return cISControlRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CISControl> findOne(Long id) {
        log.debug("Request to get CISControl : {}", id);
        return cISControlRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CISControl : {}", id);
        cISControlRepository.deleteById(id);
    }
}
