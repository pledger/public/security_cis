package eu.pledgerproject.security_cis.service.impl;

import eu.pledgerproject.security_cis.service.CISReportService;
import eu.pledgerproject.security_cis.domain.CISReport;
import eu.pledgerproject.security_cis.repository.CISReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CISReport}.
 */
@Service
@Transactional
public class CISReportServiceImpl implements CISReportService {

    private final Logger log = LoggerFactory.getLogger(CISReportServiceImpl.class);

    private final CISReportRepository cISReportRepository;

    public CISReportServiceImpl(CISReportRepository cISReportRepository) {
        this.cISReportRepository = cISReportRepository;
    }

    @Override
    public CISReport save(CISReport cISReport) {
        log.debug("Request to save CISReport : {}", cISReport);
        return cISReportRepository.save(cISReport);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CISReport> findAll(Pageable pageable) {
        log.debug("Request to get all CISReports");
        return cISReportRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CISReport> findOne(Long id) {
        log.debug("Request to get CISReport : {}", id);
        return cISReportRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CISReport : {}", id);
        cISReportRepository.deleteById(id);
    }
}
