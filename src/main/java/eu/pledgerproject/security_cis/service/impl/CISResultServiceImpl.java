package eu.pledgerproject.security_cis.service.impl;

import eu.pledgerproject.security_cis.service.CISResultService;
import eu.pledgerproject.security_cis.domain.CISResult;
import eu.pledgerproject.security_cis.repository.CISResultRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CISResult}.
 */
@Service
@Transactional
public class CISResultServiceImpl implements CISResultService {

    private final Logger log = LoggerFactory.getLogger(CISResultServiceImpl.class);

    private final CISResultRepository cISResultRepository;

    public CISResultServiceImpl(CISResultRepository cISResultRepository) {
        this.cISResultRepository = cISResultRepository;
    }

    @Override
    public CISResult save(CISResult cISResult) {
        log.debug("Request to save CISResult : {}", cISResult);
        return cISResultRepository.save(cISResult);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CISResult> findAll(Pageable pageable) {
        log.debug("Request to get all CISResults");
        return cISResultRepository.findAll(pageable);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CISResult> findOne(Long id) {
        log.debug("Request to get CISResult : {}", id);
        return cISResultRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CISResult : {}", id);
        cISResultRepository.deleteById(id);
    }
}
