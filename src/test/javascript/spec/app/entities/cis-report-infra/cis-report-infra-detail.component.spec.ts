import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SecurityCisTestModule } from '../../../test.module';
import { CISReportInfraDetailComponent } from 'app/entities/cis-report-infra/cis-report-infra-detail.component';
import { CISReportInfra } from 'app/shared/model/cis-report-infra.model';

describe('Component Tests', () => {
  describe('CISReportInfra Management Detail Component', () => {
    let comp: CISReportInfraDetailComponent;
    let fixture: ComponentFixture<CISReportInfraDetailComponent>;
    const route = ({ data: of({ cISReportInfra: new CISReportInfra(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SecurityCisTestModule],
        declarations: [CISReportInfraDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(CISReportInfraDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CISReportInfraDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load cISReportInfra on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cISReportInfra).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
