import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { SecurityCisTestModule } from '../../../test.module';
import { CISReportInfraComponent } from 'app/entities/cis-report-infra/cis-report-infra.component';
import { CISReportInfraService } from 'app/entities/cis-report-infra/cis-report-infra.service';
import { CISReportInfra } from 'app/shared/model/cis-report-infra.model';

describe('Component Tests', () => {
  describe('CISReportInfra Management Component', () => {
    let comp: CISReportInfraComponent;
    let fixture: ComponentFixture<CISReportInfraComponent>;
    let service: CISReportInfraService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SecurityCisTestModule],
        declarations: [CISReportInfraComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc',
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc',
                })
              ),
            },
          },
        ],
      })
        .overrideTemplate(CISReportInfraComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CISReportInfraComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CISReportInfraService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CISReportInfra(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cISReportInfras && comp.cISReportInfras[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CISReportInfra(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.cISReportInfras && comp.cISReportInfras[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
