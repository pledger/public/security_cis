import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { SecurityCisTestModule } from '../../../test.module';
import { CISReportInfraUpdateComponent } from 'app/entities/cis-report-infra/cis-report-infra-update.component';
import { CISReportInfraService } from 'app/entities/cis-report-infra/cis-report-infra.service';
import { CISReportInfra } from 'app/shared/model/cis-report-infra.model';

describe('Component Tests', () => {
  describe('CISReportInfra Management Update Component', () => {
    let comp: CISReportInfraUpdateComponent;
    let fixture: ComponentFixture<CISReportInfraUpdateComponent>;
    let service: CISReportInfraService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SecurityCisTestModule],
        declarations: [CISReportInfraUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(CISReportInfraUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CISReportInfraUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CISReportInfraService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CISReportInfra(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CISReportInfra();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
