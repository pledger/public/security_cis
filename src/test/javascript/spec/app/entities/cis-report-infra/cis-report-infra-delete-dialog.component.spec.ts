import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SecurityCisTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { CISReportInfraDeleteDialogComponent } from 'app/entities/cis-report-infra/cis-report-infra-delete-dialog.component';
import { CISReportInfraService } from 'app/entities/cis-report-infra/cis-report-infra.service';

describe('Component Tests', () => {
  describe('CISReportInfra Management Delete Component', () => {
    let comp: CISReportInfraDeleteDialogComponent;
    let fixture: ComponentFixture<CISReportInfraDeleteDialogComponent>;
    let service: CISReportInfraService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SecurityCisTestModule],
        declarations: [CISReportInfraDeleteDialogComponent],
      })
        .overrideTemplate(CISReportInfraDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CISReportInfraDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CISReportInfraService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
